

CPMapp.factory('taskFactory', function() {

    var tasks = [];

    var criticalPath = [];

// hardcoded data for testing purposes

    var demo1 = [
        // {id: 0,name: 'Start', time: 0,dependencies: ''},
        {id: 'A',name: 'Stworzenie założeń wstępnych', time: 15,dependencies: ''},
        {id: 'B',name: 'Koncepcja',time: 30,dependencies: 'A'},
        {id: 'C',name: 'Programowanie',time: 150,dependencies: 'B'},
        {id: 'D',name: 'Testowanie, debugging',time: 90,dependencies: 'B,C'},
        {id: 'E',name: 'Tworzenie dokumentacji',time: 120,dependencies: 'B'},
        {id: 'F',name: 'Promocja',time: 90,dependencies: 'B'},
        {id: 'G',name: 'Produkcja opakowań',time: 15,dependencies: 'C,E'},
        {id: 'H',name: 'Wypalanie CD',time: 15,dependencies: 'D,E,F'},
        {id: 'I',name: 'Dystrybucja',time: 15,dependencies: 'G,H'},
    ];
    
    var demo2 = [
        // {id: 0,name: 'Start', time: 0,dependencies: ''},
        {id: 'A',name: 'A', time: 8,dependencies: ''},
        {id: 'B',name: 'B',time: 3,dependencies: ''},
        {id: 'C',name: 'C',time: 1,dependencies: 'A,B'},
        {id: 'D',name: 'D',time: 6,dependencies: 'B,C'},
        {id: 'E',name: 'E',time: 4,dependencies: 'D,C,F,G'},
        {id: 'F',name: 'F',time: 18,dependencies: 'B'},
        {id: 'G',name: 'G',time: 1,dependencies: 'A,C'}
    ];

    var demo3 = [
        // {id: 0,name: 'Start', time: 0,dependencies: ''},
        {id: 'A',name: 'A', time: 5,dependencies: ''},
        {id: 'B',name: 'B',time: 3,dependencies: 'A'},
        {id: 'C',name: 'C',time: 4,dependencies: 'A'},
        {id: 'D',name: 'D',time: 6,dependencies: 'A,C'},
        {id: 'E',name: 'E',time: 4,dependencies: 'D'},
        {id: 'F',name: 'F',time: 3,dependencies: 'B,D,E'},
    ];

    var factory = {};

    factory.getDemo = function (demo_number) {
        switch(demo_number) {
        case 1: tasks = demo1; return tasks; break;
        case 2: tasks = demo2; return tasks; break;
        case 3: tasks = demo3; return tasks; break;
        };
    }

    factory.getTasks = function() {
        return tasks;
    }

    factory.addTask = function (tid, tname, ttime, tdep) {
        tasks.push({
        id: String.fromCharCode(tid+64),
        name: tname,
        time: ttime,
        dependencies: tdep
        });
    }

    factory.clearTasks = function () {
        tasks = [];
    }

    factory.setCriticalPath = function(path) {
        criticalPath = path;
    }

    factory.returnCriticalPath = function() {
        return criticalPath;
    }

    return factory;
    });