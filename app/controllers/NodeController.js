    /**************************************************
              RYSOWANIE SIECI
    /**************************************************/

  CPMapp.controller('NodeController', function ($scope, taskFactory){

    tasks = [];

    //initGraph();

    $scope.initGraph = function() {

      console.log('initGraph()');
      var nodes, edges = [];
      var container = '';
      var data = {};
      var options = {};
      var network = {};
      var cPath = [];
      
      tasks =  taskFactory.getTasks();

      nodes = new vis.DataSet(tasksToNodes());

      // create an array with edges
      edges = new vis.DataSet(tasksToEdges());
      console.log(edges);
      
      // create a network

      container = document.getElementById('mynetwork');

      // provide the data in the vis format
      data = {
          nodes: nodes,
          edges: edges
      };

      options = {  
             
          layout: {
            /*
              hierarchical: {
                  direction: "RL",
                  sortMethod: "directed",
                  nodeSpacing: 300,
                  levelSeparation: 200,
                  parentCentralization: true,
              },
              */improvedLayout: true,

          },
          
          interaction: {dragNodes :true},
          physics: {
              enabled: false
          },
      };

      // initialize your network!
      network = new vis.Network(container, data, options);
    }

      function checkAvailability(arr, val) {
        return arr.some(function(arrVal) {
          console.log("checkAvailability(): Array: " + arr + "Value: " + val);
          return val === arrVal;
        });
      }

    //funkcje
    function tasksToNodes(){

      var nodeList = [];
      var color = '#97C2FC';
      var cPath = taskFactory.returnCriticalPath();

      angular.forEach(tasks, function(task){
        if (checkAvailability(cPath, task.id)){
          color = '#FB7E81';
        } else {
          color = '#97C2FC'
        };

        nodeList.push({
            'id': task.id.charCodeAt()-65,
            'label': task.id + ": " + task.name,
            'color': color,
        });
      });

      color = '#97C2FC';

      console.log(nodeList);

      return nodeList;
    };

    function tasksToEdges(){
      
      var edgeList = [];
      var cPath = taskFactory.returnCriticalPath();

      angular.forEach(tasks, function(task){

        _dep = [];

        if (task['dependencies'] == undefined) {
          task['dependencies'] = '';
        }

        if(task['dependencies'].length != 0){
          if(task['dependencies'].length == 1) {
            _dep = task['dependencies'].split('');
          } else {
            _dep = task['dependencies'].split(',');
          }

          angular.forEach(_dep, function(dep){
            edgeList.push({
              'from': task.id.charCodeAt()-65,
              'to': dep.charCodeAt()-65,
              'arrows': 'from',
              'color' :'#97C2FC',
            });
          });
        }
      });

      console.log(edgeList);

      function colorEdges(){

        var fromList = [];

        angular.forEach(edgeList, function(edge){
          if (checkAvailability(cPath, String.fromCharCode(edge.from+65)) && !checkAvailability(fromList, String.fromCharCode(edge.from+65)))
          {
            if (checkAvailability(cPath, String.fromCharCode(edge.to+65)))
            {
              fromList.push(String.fromCharCode(edge.from+65));
              console.log("fromList: " + fromList);
              edge.color = '#FB7E81';
              
            }
          }
        });
      };

      function colorEdges2(){
        //convert nodes to edges
        var edgesToColor = cPath;

        //counter for the colored edges
        var count = 0;

        //change letters to node numbers
        function convertElement(element,index,array){
          console.log(element.charCodeAt()-65);
          return element.charCodeAt() - 65;
        }

        //przepisz tablice na nody
        var edgesToColorNumbers = edgesToColor.map(convertElement);

        console.log("edgesToColor: " + edgesToColorNumbers);

        var limiter = edgesToColorNumbers.length-1;
        //search for edges to change color
        angular.forEach(edgeList, function(edge){
          if (count <= limiter){
            if (edge.to == edgesToColorNumbers[count] && edge.from == edgesToColorNumbers[count+1]){
              count++;
              edge.label = count;
              edge.color = '#FB7E81';
            }
          }
        // console.log()
        });
      };


      colorEdges2();

    /* Sortowanie nie jest potrzebne 
      function sortByKey(array, key) {
          return array.sort(function(a, b) {
              var x = a[key]; var y = b[key];
              return ((x < y) ? -1 : ((x > y) ? 1 : 0));
          });
      };

      var sortedList = [];

      sortedList = sortByKey(edgeList,'to');

      console.log(edgeList);
      console.log(sortedList);
    */
      return edgeList;
      
    }
    
  }); //CONTROLLER END  