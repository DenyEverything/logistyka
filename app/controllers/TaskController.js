CPMapp.controller('TaskController', function ($scope, taskFactory ){

    $scope.tasks = [];

    var counter = 0;

    $scope.init = function() {
        $scope.tasks = taskFactory.getTasks();
        counter = $scope.tasks.length;
    };

    $scope.getDemo = function(id){
        $scope.tasks = taskFactory.getDemo(id);
        counter = $scope.tasks.length;
    }

    $scope.addTask = function(){

        console.log($scope.taskDependencies);

        //check if new task needs dependency
        if ($scope.taskDependencies != null ) {

        if ($scope.taskDependencies !== undefined && $scope.taskDependencies !== '') {

            //get dependencies entered to taskDependencies field
            var dependencies = $scope.taskDependencies.split(',');
            
            //check for wrong dependencies
            var _deps = dependencies.map(function (dep){

            for (j=0;j<$scope.tasks.length;j++){
                //console.log($scope.tasks[j].id);
                if (dep == $scope.tasks[j].id)
                return true;
            }    
            return false;
            });

            //console.log(_deps);

            //verify all dependencies 
            for (i=0; i<_deps.length;i++){
            if(_deps[i] === false) return alert("brak takich Zależności");
            }
        }
        }

        counter++;
        
        $scope.tasks.push({
        'id': String.fromCharCode(64+counter),
        'name': $scope.taskName,
        'time': $scope.taskTime,
        'dependencies': $scope.taskDependencies
        });

        console.log(taskFactory.getTasks());
        taskFactory.addTask(counter, $scope.taskName, $scope.taskTime, $scope.taskDependencies);
        console.log(taskFactory.getTasks());
        //clear entry fields
        $scope.taskName = '';
        $scope.taskTime = '';
        $scope.taskDependencies = '';

    };

    $scope.removeTask = function( idx ){

        //store removed ID
        var removedID = $scope.tasks[idx].id; 

        //remove task with given ID
        $scope.tasks.splice(idx,1);
        
        //checking deprecated dependencies and removing them from tasks
        angular.forEach($scope.tasks, function(task){

        console.log(
        '\ntask: \t' + task['id'] +
        '\nidx: \t' + removedID );

        var _dep = [];

        if (task['dependencies'] !== undefined && task['dependencies'] !== '') {

            if(task['dependencies'].length != 0){ 
            
            if(task['dependencies'].length == 1) {
                _dep = task['dependencies'].split('');
            } else {
                _dep = task['dependencies'].split(',');
            }

            for (i=0; i < _dep.length; i++){
                if (_dep[i] == removedID) {
                console.log(
                    
                    "\ntask['dependencies']: \t" + task['dependencies'] +
                    '\n_dep: \t' + _dep +
                    '\ni: \t' + i 
                    
                    );
                _dep.splice(i,1);
                
                }
                task['dependencies'] = _dep.toString();
            }
            }
        }
        });
    

        // DEBUG //
        //---------------------
        angular.forEach($scope.tasks, function(task){
        console.log(task);
        });
        //---------------------
    };

    $scope.clearAll = function() {

        // DEBUG //
        //---------------------
        angular.forEach($scope.tasks, function(task){
        console.log(task);
        });
        //---------------------

        $scope.tasks = [];

        $scope.taskName = '';
        $scope.taskTime = '';
        $scope.taskDependencies = '';

        taskFactory.clearTasks();
        counter = 0;

    };

    $scope.getPath = function(){
        //set task as Activities
        var list = new ActivityList();

        //get the id of the last node
        var lastTaskId = String.fromCharCode(64+$scope.tasks.length);

        angular.forEach($scope.tasks, function(task){

        //check for all dependencies for the current processed task
        _dep = [];
        if (task['dependencies'] == undefined) {
            task['dependencies'] = '';
        }

        if(task['dependencies'].length != 0){
            if(task['dependencies'].length == 1) {
            _dep = task['dependencies'].split('');
            } else {
            _dep = task['dependencies'].split(',');
            }
        }

        //rewrite tasks into Activities so they can be used by cmp.js module
        list.addActivity(new Activity({
            id: task.id,
            duration: task.time,
            predecessors: _dep,
        }));
        });

    console.log(lastTaskId);

    console.log('TABLE', list.getList());



    var path = list.getCriticalPath(lastTaskId);

    path = getFilteredPath(myPath);

    console.log(path);

    //save the calculated critical path to factory
    taskFactory.setCriticalPath(path);

}
});